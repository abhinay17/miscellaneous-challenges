##### S3 APS Links

with open("C:\\Users\\Abhinay Reddy\\Desktop\\decompression_aps.txt", 'rt') as in_file:
    contents = in_file.read()  # Read the entire file into a variable named contents.

desired_content = contents.split("article")  # Print contents

##### Sujith's Excel Sheet
import numpy as np
import pandas as pd

name_strings = []

classified_abstracts = pd.read_excel("C:\\Users\\Abhinay Reddy\\Downloads\\Preliminary_Classified_APSSampleData.xlsx")

names = classified_abstracts['Document Name'].tolist()
for name in names:
    name_strings.append(name[:-4])

mapped_link = []
###Mapping
for name in name_strings:
    for content in desired_content:
        if name in content:
            mapped_link.append(content.split("available")[0])

# print(mapped_link[0])
# print(len(mapped_link))

### Adding new column to the dataframe

classified_abstracts['URL'] = pd.Series(mapped_link).values

#print(classified_abstracts.head(3))

# DF TO CSV
classified_abstracts.to_csv('url_abstracts_final.csv', index = False)



#print(name_strings[0] in 'http:~~meetings.aps.org~Meeting~DFD05~Session~BQ.1 available @ data/2018/05/15/unstructured/processed/aps/decompressed/0f779114-615d-50cd-8e94-8ba16b80c9b7')